package com.river.classbaithuchanh1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonCalculate;
    EditText editTextHeight;
    EditText editTextWeight;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewResult.setText("" + Double.parseDouble(editTextWeight.getText().toString()) / (Double.parseDouble(editTextHeight.getText().toString()) * Double.parseDouble(editTextHeight.getText().toString())));
            }
        });
    }

    private void setView() {
        buttonCalculate = findViewById(R.id.button_calculate);
        editTextHeight = findViewById(R.id.editText_height);
        editTextWeight = findViewById(R.id.editText_weight);
        textViewResult = findViewById(R.id.textView_result);
    }
}